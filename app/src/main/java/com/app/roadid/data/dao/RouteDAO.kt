package com.app.roadid.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.app.roadid.data.model.Route

@Dao
interface RouteDAO {
    @Query("SELECT * from route WHERE id =:id")
    fun getById(id: Int): Route

    @Query("SELECT * from route")
    fun getAll(): List<Route>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(song: Route)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(song: List<Route>)

    @Query("DELETE from route")
    fun deleteAll()

    @Query("DELETE from route WHERE id = :id")
    fun delete(id: Int)
}