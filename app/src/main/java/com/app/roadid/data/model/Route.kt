package com.app.roadid.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

@Entity(tableName = "route")
data class Route(

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,

    @ColumnInfo(name = "start_lat")
    var startLat: Double,

    @ColumnInfo(name = "start_long")
    var startLong: Double,

    @ColumnInfo(name = "end_lat")
    var endLat: Double,

    @ColumnInfo(name = "end_long")
    var endLong: Double,

    @ColumnInfo(name = "rate")
    var rate: Int,

    @ColumnInfo(name = "message")
    var message: String,

    @ColumnInfo(name = "author")
    var author: String

) {

    constructor(
        startLat: Double,
        startLong: Double,
        endLat: Double,
        endLong: Double,
        rate: Int,
        message: String,
        author: String,
        marker: Marker?
    ) : this(0, startLat, startLong, endLat, endLong, rate, message, author)

    constructor() :
            this( 0, 0.0, 0.0,0.0, 0.0, 0, "", "")

}