package com.app.roadid.data.repository

import com.app.roadid.data.dao.RouteDAO
import com.app.roadid.data.model.Route
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RouteRepositiry(private val dao: RouteDAO) {

    fun getAll() = Observable.fromCallable { dao.getAll() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun insert(route: Route) = Observable.fromCallable { dao.insert(route) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun delete(id:Int) = Observable.fromCallable { dao.delete(id) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getById(id: Int) = Observable.fromCallable { dao.getById(id) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}