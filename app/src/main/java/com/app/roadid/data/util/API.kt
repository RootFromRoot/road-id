package com.app.roadid.data.util

import com.app.roadid.data.model.GoogleMapDTO
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber

interface API {
    @GET("json")
    fun getRoute(
        @Query("origin", encoded = true) originPoint: String,
        @Query("destination", encoded = true) destinationPoint: String,
        @Query("sensor") sensor: Boolean,
        @Query("mode") mode: String,
        @Query("key") key: String
    ): Observable<GoogleMapDTO>

    companion object {
        private val retrofit: Retrofit = Retrofit.Builder()
            .client(OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger
                { message -> Timber.i(message) }).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .build()
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://maps.googleapis.com/maps/api/directions/")
            .build()

        fun get(): API {
            return retrofit.create(API::class.java)
        }
    }
}