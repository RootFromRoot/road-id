package com.app.roadid.data.util

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.app.roadid.data.dao.RouteDAO
import com.app.roadid.data.model.Route

@Database(entities = [Route::class], version = 4)
//@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun routeDao(): RouteDAO
}