package com.app.roadid.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.app.roadid.R
import com.app.roadid.data.util.RC_SIGN_IN
import com.app.roadid.ui.presenter.MainActivityImpl
import com.app.roadid.ui.presenter.MainActivityPresenter
import com.app.roadid.ui.view.MainActivityView
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : AppCompatActivity(), MainActivityView {
    private val presenter: MainActivityPresenter = MainActivityImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.plant(Timber.DebugTree())
        setupView()
    }

    override fun setupView() {
        setContentView(R.layout.activity_main)
        presenter.configureGoogleSignIn(this)

        google_button.setOnClickListener {
            startActivity(Intent(this, MapActivity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            startActivity(Intent(this@MainActivity, MapActivity::class.java))
        } else {
            Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
        }
    }
}
