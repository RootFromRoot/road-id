package com.app.roadid.ui.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.app.roadid.R
import com.app.roadid.ui.presenter.GoogleMapPresenter
import com.app.roadid.ui.presenter.GoogleMapPresenterImpl
import com.app.roadid.ui.view.MapActivityView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.popup_message.view.*

class MapActivity : AppCompatActivity(), MapActivityView, OnMapReadyCallback {
    private val presenterMap: GoogleMapPresenter = GoogleMapPresenterImpl()
    lateinit var googleMap: GoogleMap
    lateinit var view: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        presenterMap.bind(this)
        setupView()
        inflateGoogleMap()
    }

    override fun inflateGoogleMap() {
        (supportFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment).getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap!!
        presenterMap.setupLocationService(googleMap)
    }

    override fun setupView() {
        fab_edit.setOnClickListener {
            tv_road_state_info.text = "Виберите участок дороги для обозначения его состояния"
            fab_save.visibility = View.VISIBLE
            fab_edit.visibility = View.GONE

            presenterMap.attachOnMapClickListener(googleMap)
        }
        fab_save.setOnClickListener {
            tv_road_state_info.text = "Для записи информации о участке дороги перейдите в режим записи"
            fab_edit.visibility = View.VISIBLE
            fab_save.visibility = View.GONE
            presenterMap.drawRouteFromRouteList(googleMap)
        }
    }

    override fun showPopUp(author: String, feedbackMessage: String) {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.popup_message, null)

        val popupWindow =
            PopupWindow(view, 556, LinearLayout.LayoutParams.WRAP_CONTENT)
        TransitionManager.beginDelayedTransition(wrapper)
        popupWindow.showAtLocation(wrapper, Gravity.CENTER, 0, 0)
        view.tv_message.text = feedbackMessage
        view.tv_author.text = author
        view.btn_close.setOnClickListener {
            popupWindow.dismiss()
        }
    }
}


