package com.app.roadid.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.roadid.R
import com.app.roadid.ui.activity.MainActivity
import com.app.roadid.ui.activity.MapActivity
import com.app.roadid.ui.presenter.GoogleMapPresenter
import com.app.roadid.ui.presenter.GoogleMapPresenterImpl
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.popup_post_feedback.view.*
import timber.log.Timber


class FeedBackDialog : DialogFragment() {
    interface OnActionListener {
        fun onOkClickListener(author: String, message: String, rate: Int)
    }

    private lateinit var rootView: View
    private var listener: OnActionListener? = null
    private var rate: Int = 0

    fun setOnActionListener(listener: OnActionListener) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.popup_post_feedback, container, false)
        rootView.numberPicker.maxValue = 5
        rootView.numberPicker.minValue = 3

        rootView.numberPicker.setOnValueChangedListener { numberPicker, oldVal, newVal ->
            rate = newVal
        }

        rootView.btn_cancel.setOnClickListener {
            this.dismiss()
        }

        rootView.btn_ok.setOnClickListener {
            listener?.onOkClickListener(
                rootView.et_author_name.text.toString(),
                rootView.et_message.text.toString(),
                rate
            )

            this.dismiss()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        dialog.window.setLayout(
            resources.getDimension(R.dimen.popup_width).toInt(),
            resources.getDimension(R.dimen.popup_height).toInt()
        )
    }

    companion object {
        fun newInstance(locationList: ArrayList<LatLng>): FeedBackDialog {
            val dialog = FeedBackDialog()
            val args = Bundle()

            dialog.arguments = args
            return dialog
        }
    }

}