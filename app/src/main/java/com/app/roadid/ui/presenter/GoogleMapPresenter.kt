package com.app.roadid.ui.presenter

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.app.roadid.data.model.GoogleMapDTO
import com.app.roadid.data.model.Route
import com.app.roadid.data.repository.RouteRepositiry
import com.app.roadid.data.util.API
import com.app.roadid.data.util.API_GOOGLE_KEY
import com.app.roadid.data.util.Application
import com.app.roadid.data.util.LOCATION_PERMISSION_REQUEST_CODE
import com.app.roadid.ui.activity.MapActivity
import com.app.roadid.ui.dialog.FeedBackDialog
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

interface GoogleMapPresenter {
    var currentLatLng: LatLng

    fun bind(activity: MapActivity)
    fun setupMap(googleMap: GoogleMap)
    fun attachOnMapClickListener(googleMap: GoogleMap)
    fun getDirectionURL(origin: LatLng, dest: LatLng): String
    fun setupLocationService(googleMap: GoogleMap)
    fun addRouteToMap()
    fun drawRouteFromRouteList(googleMap: GoogleMap)
}

@SuppressLint("CheckResult")
class GoogleMapPresenterImpl : GoogleMapPresenter, GoogleMap.OnMarkerClickListener, FeedBackDialog.OnActionListener {
    override lateinit var currentLatLng: LatLng

    private lateinit var activity: MapActivity
    private lateinit var repository: RouteRepositiry
    private val requestInterface = API.get()

    private val routeList: ArrayList<Route> = ArrayList()
    private val location1 = LatLng(50.450548, 30.522915)
    private val location2 = LatLng(51.450548, 31.522915)
    private val location3 = LatLng(50.514911, 30.447142)
    private val location4 = LatLng(50.433232, 30.432587)
    private val location5 = LatLng(50.437513, 30.418870)

    var receivedFromFragmentAuthor: String = ""
    var receivedFromFragmentMessage: String = ""
    var receivedFromFragmentRate: Int = 0

    var routeWithMarker: HashMap<Marker, Route> = HashMap()

    var markerListForCreatingFeedBack: ArrayList<Marker> = ArrayList()


    private val locationList: ArrayList<LatLng> = ArrayList()

    override fun bind(activity: MapActivity) {
        this.activity = activity
        repository = RouteRepositiry((activity.application as Application).db.routeDao())
    }

    override fun setupMap(googleMap: GoogleMap) {
        setRoute(googleMap)
        googleMap.setOnMarkerClickListener {
            onMarkerClick(it)
        }
        drawRouteFromRouteList(googleMap)
    }

    override fun drawRouteFromRouteList(googleMap: GoogleMap) {
        repository.getAll().subscribe {
            routeList.addAll(it)
            routeList.forEach { route ->
                routeWithMarker.put(
                    addMarkerToMap(
                        LatLng(route.startLat, route.startLong),
                        route.author, googleMap
                    ), route
                )
                decodeRouteToLatLngList(route) {latLngList ->
                    drawLatLngListOnGoogleMap(route, latLngList, googleMap)
                }
            }
        }
    }

    override fun attachOnMapClickListener(googleMap: GoogleMap) {
        googleMap.setOnMapLongClickListener {
            if (markerListForCreatingFeedBack.size > 1) {
                Toast.makeText(activity, "Two points is selected", Toast.LENGTH_SHORT).show()
                return@setOnMapLongClickListener
            }
            locationList.add(it)
            markerListForCreatingFeedBack.add(addMarkerToMap(it, "Loc", googleMap))

            if (markerListForCreatingFeedBack.size == 2) {
                openFragment(locationList)
            }
        }
    }

    override fun onOkClickListener(author: String, message: String, rate: Int) {
        receivedFromFragmentAuthor = author
        receivedFromFragmentMessage = message
        receivedFromFragmentRate = rate
        addRouteToMap()
    }

    override fun addRouteToMap() {
        Timber.i(locationList.toString())
        val localRoute = (Route(
            id = null,
            startLat = locationList[0].latitude,
            startLong = locationList[0].longitude,
            endLat = locationList[1].latitude,
            endLong = locationList[1].longitude,
            rate = receivedFromFragmentRate,
            message = receivedFromFragmentMessage,
            author = receivedFromFragmentAuthor
        ))

        routeList.add(localRoute)
        repository.insert(localRoute).subscribe()

        locationList.clear()

        drawRouteFromRouteList(activity.googleMap)
        markerListForCreatingFeedBack.clear()
    }

    private fun openFragment(locationList: ArrayList<LatLng>) {
        val newFragment = FeedBackDialog.newInstance(locationList)
        newFragment.setOnActionListener(this)
        newFragment.show(activity.supportFragmentManager, "dialog")
    }

    private fun setRoute(googleMap: GoogleMap): ArrayList<Route> {
        routeList.add(
            Route(
                null,
                location1.latitude,
                location1.longitude,
                location2.latitude,
                location2.longitude,
                3,
                "Bad Road!!",
                "Billy Lewis"
            )
        )
        routeList.add(
            Route(
                null,
                location1.latitude,
                location1.longitude,
                location3.latitude,
                location3.longitude,
                4,
                "Good road!",
                "John"
            )
        )
        routeList.add(
            Route(
                null,
                location2.latitude,
                location2.longitude,
                location3.latitude,
                location3.longitude,
                5,
                "Класс!",
                "Гриша"
            )
        )
        routeList.add(
            Route(
                null,
                location4.latitude,
                location4.longitude,
                location5.latitude,
                location5.longitude,
                5,
                "Класс!",
                "Гриша"
            )
        )
        return routeList
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        routeWithMarker.forEach {
            if (p0 == it.key) {
                openPopUp(it.value.author, it.value.message)
                     }
        }
        return true
    }

    private fun openPopUp(author: String, feedbackMessage: String) {
        activity.showPopUp(author, feedbackMessage)
    }

    private fun decodeRouteToLatLngList(route: Route, onSuccess: (ArrayList<LatLng>) -> Unit) {
        fetchRouteFromNetwork(route).subscribe({ onRouteSuccessLoaded(it, onSuccess) }, ::onRouteLoadingFailed)
    }

    private fun onRouteSuccessLoaded(googleMapDTO: GoogleMapDTO, onSuccess: (ArrayList<LatLng>) -> Unit) {
        onSuccess(ArrayList<LatLng>().apply {
            for (i in 0..(googleMapDTO.routes[0].legs[0].steps.size - 1)) {
                addAll(decodePolyline(googleMapDTO.routes[0].legs[0].steps[i].polyline.points))
            }
        })
    }

    private fun onRouteLoadingFailed(throwable: Throwable) {
        Timber.e(throwable.toString())
    }

    private fun fetchRouteFromNetwork(route: Route): Observable<GoogleMapDTO> {
        return requestInterface.getRoute(
            "${route.startLat},${route.startLong}",
            "${route.endLat},${route.endLong}",
            false,
            "driving",
            API_GOOGLE_KEY
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    private fun drawLatLngListOnGoogleMap(route: Route, latLngList: List<LatLng>, googleMap: GoogleMap) {
        googleMap.addPolyline(PolylineOptions().apply {
            addAll(latLngList)
            width(10f)
            geodesic(true)
            setColorByRate(route, this)
        })
    }

    private fun addMarkerToMap(location: LatLng, title: String, googleMap: GoogleMap) =
        googleMap.addMarker(MarkerOptions().position(location).title(title))

    private fun setColorByRate(route: Route, polylineOptions: PolylineOptions) =
        when (route.rate) {
            3 -> polylineOptions.color(Color.RED)
            4 -> polylineOptions.color(Color.YELLOW)
            5 -> polylineOptions.color(Color.GREEN)
            else -> polylineOptions.color(Color.RED)
        }

    private fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val latLng = LatLng((lat.toDouble() / 1E5), (lng.toDouble() / 1E5))
            poly.add(latLng)
        }
        return poly
    }

    private fun requestForPermission() {
        if (ActivityCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
    }

    @SuppressLint("MissingPermission")
    override fun setupLocationService(googleMap: GoogleMap) {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        requestForPermission()

        googleMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                setupMap(googleMap)
            }
        }
    }

    override fun getDirectionURL(origin: LatLng, dest: LatLng): String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude}," +
                "${origin.longitude}&destination=${dest.latitude},${dest.longitude}" +
                "&sensor=false&mode=driving&key=$API_GOOGLE_KEY"
    }
}