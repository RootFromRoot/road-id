package com.app.roadid.ui.presenter

import android.content.Intent
import com.app.roadid.data.util.RC_SIGN_IN
import com.app.roadid.ui.activity.MainActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

interface MainActivityPresenter {
    var mGoogleSignInClient: GoogleSignInClient
    var mGoogleSignInOptions: GoogleSignInOptions

    fun signIn(activity: MainActivity)
    fun configureGoogleSignIn(activity: MainActivity)

}

class MainActivityImpl : MainActivityPresenter {
    override lateinit var mGoogleSignInClient: GoogleSignInClient
    override lateinit var mGoogleSignInOptions: GoogleSignInOptions

    override fun signIn(activity: MainActivity) {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        activity.startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun configureGoogleSignIn(activity: MainActivity) {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("19756180757-g0uttejjs97p7641c9s2tmrcfdo3mjfq.apps.googleusercontent.com")
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(activity, mGoogleSignInOptions)
    }
}