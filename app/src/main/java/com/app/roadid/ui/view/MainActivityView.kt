package com.app.roadid.ui.view

interface MainActivityView {
    fun setupView()
}