package com.app.roadid.ui.view

interface MapActivityView {
    fun inflateGoogleMap()
    fun setupView()
    fun showPopUp(author: String, feedbackMessage: String)

}